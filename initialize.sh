#!/bin/bash
function printSeparator(){
echo -e "\n############################################\n"
}
if [ "$EUID" -ne 0 ]
  then echo "Must be run as root!"
  exit
fi
apt update -y
if [ -x "$(command -v docker)" ]; then
    echo "Docker Has Already Been Installed."
    docker -v
else
	echo "Install Latest Stable Docker Release"
	apt-get install -y apt-transport-https ca-certificates curl gnupg-agent software-properties-common
	curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
	add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
	apt-get update -y
	apt-get install -y docker-ce docker-ce-cli containerd.io nvidia-container-runtime
	GPU_ID=$(nvidia-smi --format=csv,noheader --query-gpu=uuid)
	if [ ${#GPU_ID} -ge 40 ]; then echo "Failed to get GPU UUID" ; exit
	tee /etc/docker/daemon.json >/dev/null <<EOF
{
 "runtimes": {
    "nvidia": {
      "path": "/usr/bin/nvidia-container-runtime",
      "runtimeArgs": []
    }
    },
    "default-runtime": "nvidia",
    "node-generic-resources": [
    "gpu=$GPU_ID"
    ],
  "exec-opts": ["native.cgroupdriver=systemd"],
  "log-driver": "json-file",
  "log-opts": {
    "max-size": "100m"
  },
  "storage-driver": "overlay2"
}
EOF
	mkdir -p /etc/systemd/system/docker.service.d
	groupadd docker
	MAINUSER=$(logname) 
	usermod -aG docker $MAINUSER
	systemctl daemon-reload
	systemctl restart docker
	echo "Docker Installation Done"
fi

if [ -x "$(command -v docker-compose)" ]; then
    echo "Docker Compose Has Already Been Installed."
    docker-compose -v

else
	echo "Install Latest Stable Docker Compose Release."
	COMPOSEVERSION=$(curl -s https://github.com/docker/compose/releases/latest/download 2>&1 | grep -Po [0-9]+\.[0-9]+\.[0-9]+)
	curl -L "https://github.com/docker/compose/releases/download/$COMPOSEVERSION/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
	chmod +x /usr/local/bin/docker-compose
	ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose
	echo "Docker Compose Installation Done"
fi	


docker swarm init
docker node update --label-add ingress=true $(hostname)

printSeparator
read -p "Enter New Database User Name: "  db_username && \
echo "$db_username" | docker secret create DB_USERNAME -

printSeparator
read -p "Enter Database User Password: "  db_password && \
echo "$db_password" | docker secret create DB_PASSWORD -

export $(cat .env | grep REPOSITORY)
printSeparator
echo "Enter docker registry username and password"
retval=1
while [ $retval -ne 0 ]
do
	docker login $REPOSITORY
	retval=$?
done

MAINUSER=$(logname) 
chown $MAINUSER:$MAINUSER /home/$MAINUSER/.docker/config.json